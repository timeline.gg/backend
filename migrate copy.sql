-- TABLA DE USUARIOS
create table "users" (
  "id" bigserial primary key not null,
  "name" varchar(255) not null,
  "email" varchar(255) not null,
  "email_verified_at" timestamp(0) without time zone null,
  "password" varchar(255) not null,
  "last_login" timestamp(0) without time zone null,
  "remember_token" varchar(100) null,
  "created_at" timestamp(0) without time zone null,
  "updated_at" timestamp(0) without time zone null
);

-- RESTRICCIÓN DE EMAIL ÚNICO
alter table "users" add constraint "users_email_unique" unique ("email");

create table "failed_jobs" (
    "id" bigserial primary key not null,
    "connection" text not null,
    "queue" text not null,
    "payload" text not null,
    "exception" text not null,
    "failed_at" timestamp(0) without time zone default CURRENT_TIMESTAMP not null
);

-- TABLA DE COMPARACIONES
create table "comparisons" (
    "id" bigserial primary key not null,
    "user_id" bigint not null,
    "name" varchar(255) not null,
    "description" varchar(255) not null,
    "review" varchar(255) not null,
    "rating" smallint not null,
    "schema" jsonb not null
);

-- CLAVES FORÁNEAS DE COMPARACÍON
alter table "comparisons" add   constraint "comparisons_user_id_foreign" foreign key ("user_id") references "users" ("id");

-- TABLA DE DATOS DE ENTIDADES
create table "entity_data" (
    "id" bigserial primary key not null,
    "user_id" bigint not null,
    "name" varchar(255) not null,
    "public" boolean not null,
    "data" jsonb not null,
    "created_at" timestamp(0) without time zone null,
    "updated_at" timestamp(0) without time zone null
);

-- CLAVES FORÁNEAS DE DATOS DE ENTIDADES
alter table "entity_data" add  constraint "entity_data_user_id_foreign" foreign key ("user_id") references "users" ("id");

-- TABLA DE ESQUEMAS DE ENTIDADES
create table "entity_schemas" (
    "id" bigserial primary key not null,
    "user_id" bigint not null,
    "name" varchar(255) not null,
    "description" varchar(255) not null,
    "parent" bigint not null,
    "schema" jsonb not null
);

-- CLAVES FORÁNEAS DE ESQUEMAS DE ENTIDADES
alter table "entity_schemas" add constraint "entity_schemas_user_id_foreign" foreign key ("user_id") references "users" ("id");
alter table "entity_schemas" add constraint "entity_schemas_parent_foreign" foreign key ("parent") references "entity_schemas" ("id");

-- TABLA DE VALORACIONES
create table "comparison_user_ratings" (
    "comparison_id" bigint not null,
    "user_id" bigint not null,
    "rating" smallint not null
);

-- CLAVES FORÁNEAS DE VALORACIONES
alter table "comparison_user_ratings" add constraint "comparison_user_ratings_comparison_id_foreign" foreign key ("comparison_id") references "comparisons" ("id");
alter table "comparison_user_ratings" add constraint "comparison_user_ratings_user_id_foreign" foreign key ("user_id") references "users" ("id"); 

-- RESTRICCIÓN DE VALORACIÓN ÚNICA ENTRE USUARIO Y COMPARACIÓN
alter table "comparison_user_ratings" add constraint "comparison_user_ratings_comparison_id_user_id_unique" unique ("comparison_id", "user_id");

-- TABLA DE RELACIÓN ENTRE COMPARACIÓN Y ENTIDAD
create table "comparison_entity_data" (
    "comparison_id" bigint not null,
    "entity_id" bigint not null,
    "weight" smallint not null
);

-- CLAVES FORÁNEAS Y RESTRICCIÓN DE CLAVE ÚNICA
alter table "comparison_entity_data" add constraint "comparison_entity_data_entity_id_comparison_id_unique" unique ("entity_id", "comparison_id");
alter table "comparison_entity_data" add constraint "comparison_entity_data_comparison_id_foreign" foreign key ("comparison_id") references "comparisons" ("id");
alter table "comparison_entity_data" add constraint "comparison_entity_data_entity_id_foreign" foreign key ("entity_id") references "entity_data" ("id");

-- TABLA DE RELACIÓN ENTRE ENTIDAD Y ESQUEMA
create table "entity_data_entity_schema" (
    "entity_id" bigint not null,
    "schema_id" bigint not null
);
-- RESTRICCIÓN DE CLAVE ÚNICA
alter table "entity_data_entity_schema" add constraint "entity_data_entity_schema_entity_id_schema_id_unique" unique ("entity_id", "schema_id");