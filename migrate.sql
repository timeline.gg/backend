CreateUsersTable: create table "users" (
    "id" bigserial primary key not null,
    "name" varchar(255) not null,
    "email" varchar(255) not null,
    "image" varchar(255) null,
    "email_verified_at" timestamp(0) without time zone null,
    "password" varchar(255) not null,
    "active" boolean not null default '0',
    "activation_token" varchar(255) not null,
    "last_login" timestamp(0) without time zone null,
    "remember_token" varchar(100) null,
    "created_at" timestamp(0) without time zone null,
    "updated_at" timestamp(0) without time zone null,
    "deleted_at" timestamp(0) without time zone null
) CreateUsersTable:
alter table "users"
add constraint "users_email_unique" unique ("email") CreateOauthAuthCodesTable: create table "oauth_auth_codes" (
        "id" varchar(100) not null,
        "user_id" bigint not null,
        "client_id" bigint not null,
        "scopes" text null,
        "revoked" boolean not null,
        "expires_at" timestamp(0) without time zone null
    ) CreateOauthAuthCodesTable:
alter table "oauth_auth_codes"
add primary key ("id") CreateOauthAuthCodesTable: create index "oauth_auth_codes_user_id_index" on "oauth_auth_codes" ("user_id") CreateOauthAccessTokensTable: create table "oauth_access_tokens" (
        "id" varchar(100) not null,
        "user_id" bigint null,
        "client_id" bigint not null,
        "name" varchar(255) null,
        "scopes" text null,
        "revoked" boolean not null,
        "created_at" timestamp(0) without time zone null,
        "updated_at" timestamp(0) without time zone null,
        "expires_at" timestamp(0) without time zone null
    ) CreateOauthAccessTokensTable:
alter table "oauth_access_tokens"
add primary key ("id") CreateOauthAccessTokensTable: create index "oauth_access_tokens_user_id_index" on "oauth_access_tokens" ("user_id") CreateOauthRefreshTokensTable: create table "oauth_refresh_tokens" (
        "id" varchar(100) not null,
        "access_token_id" varchar(100) not null,
        "revoked" boolean not null,
        "expires_at" timestamp(0) without time zone null
    ) CreateOauthRefreshTokensTable:
alter table "oauth_refresh_tokens"
add primary key ("id") CreateOauthClientsTable: create table "oauth_clients" (
        "id" bigserial primary key not null,
        "user_id" bigint null,
        "name" varchar(255) not null,
        "secret" varchar(100) null,
        "provider" varchar(255) null,
        "redirect" text not null,
        "personal_access_client" boolean not null,
        "password_client" boolean not null,
        "revoked" boolean not null,
        "created_at" timestamp(0) without time zone null,
        "updated_at" timestamp(0) without time zone null
    ) CreateOauthClientsTable: create index "oauth_clients_user_id_index" on "oauth_clients" ("user_id") CreateOauthPersonalAccessClientsTable: create table "oauth_personal_access_clients" (
        "id" bigserial primary key not null,
        "client_id" bigint not null,
        "created_at" timestamp(0) without time zone null,
        "updated_at" timestamp(0) without time zone null
    ) CreateFailedJobsTable: create table "failed_jobs" (
        "id" bigserial primary key not null,
        "connection" text not null,
        "queue" text not null,
        "payload" text not null,
        "exception" text not null,
        "failed_at" timestamp(0) without time zone default CURRENT_TIMESTAMP not null
    ) CreateCategoriesTable: create table "categories" (
        "id" bigserial primary key not null,
        "title" varchar(255) not null,
        "description" varchar(255) not null,
        "image" varchar(255) null,
        "created_at" timestamp(0) without time zone null,
        "updated_at" timestamp(0) without time zone null
    ) CreateCardsTable: create table "cards" (
        "id" bigserial primary key not null,
        "title" varchar(255) not null,
        "description" varchar(255) null,
        "year" integer not null,
        "image" varchar(255) null,
        "category_id" bigint not null,
        "created_at" timestamp(0) without time zone null,
        "updated_at" timestamp(0) without time zone null
    ) CreateCardsTable:
alter table "cards"
add constraint "cards_category_id_foreign" foreign key ("category_id") references "categories" ("id") CreateRankingsTable: create table "rankings" (
        "id" bigserial primary key not null,
        "user_id" bigint not null,
        "category_id" bigint not null,
        "score" integer not null
    ) CreateRankingsTable:
alter table "rankings"
add constraint "rankings_user_id_foreign" foreign key ("user_id") references "users" ("id") CreateRankingsTable:
alter table "rankings"
add constraint "rankings_category_id_foreign" foreign key ("category_id") references "categories" ("id") CreateGamesTable: create table "games" (
        "id" bigserial primary key not null,
        "user_id" bigint not null,
        "category_id" bigint not null,
        "hits" integer not null,
        "misses" integer not null,
        "score" integer not null,
        "numbre_round" integer not null,
        "start_round_time" timestamp(0) without time zone not null,
        "last_round_time" timestamp(0) without time zone not null,
        "hand_cards" json not null
    ) CreateGamesTable:
alter table "games"
add constraint "games_user_id_foreign" foreign key ("user_id") references "users" ("id") CreateGamesTable:
alter table "games"
add constraint "games_category_id_foreign" foreign key ("category_id") references "categories" ("id") CreateRolesTable: create table "roles" (
        "id" bigserial primary key not null,
        "name" varchar(255) not null,
        "description" varchar(255) not null,
        "created_at" timestamp(0) without time zone null,
        "updated_at" timestamp(0) without time zone null
    ) CreateRoleUserTable: create table "role_user" (
        "id" bigserial primary key not null,
        "role_id" bigint not null,
        "user_id" bigint not null,
        "created_at" timestamp(0) without time zone null,
        "updated_at" timestamp(0) without time zone null
    ) CreateRoleUserTable:
alter table "role_user"
add constraint "role_user_role_id_foreign" foreign key ("role_id") references "roles" ("id") on delete cascade CreateRoleUserTable:
alter table "role_user"
add constraint "role_user_user_id_foreign" foreign key ("user_id") references "users" ("id") on delete cascade