<?php

use App\User;
use App\Role;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User([
            'name'              => "admin",
            'email'             => "admin@timeline.gg",
            'password'          => bcrypt('admin'),
            'activation_token'  => '',
            'active'            => true
        ]);
        $user->save();
        $user->roles()->attach(Role::where('name', 'admin')->first());

        $user = new User([
            'name'              => "demo",
            'email'             => "demo@timeline.gg",
            'password'          => bcrypt('demo'),
            'activation_token'  => '',
            'active'            => true
        ]);
        $user->save();        
        $user->roles()->attach(Role::where('name', 'user')->first());


        factory(User::class, 50)->create()->each(function ($user) {
            $user->roles()->attach(Role::where('name', 'user')->first());
        });

    }
}
