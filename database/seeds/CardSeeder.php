<?php

use Illuminate\Support\Facades\File;
use Illuminate\Database\Seeder;
use App\Card;
use App\Category;

class CardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get("database/data/inventions.json");
        $data = json_decode($json);
        shuffle($data);
        $category = Category::where('title', 'Inventos')->first();
        foreach ($data as $obj) {
            $card = new Card();
            $card->title = $obj->title;
            $card->year = $obj->year;
            $card->category()->associate($category);
            $card->save();
        }
    }
}
