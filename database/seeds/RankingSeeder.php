<?php

use Illuminate\Support\Facades\File;
use Illuminate\Database\Seeder;
use App\Card;
use App\Category;
use App\Ranking;
use App\User;

class RankingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $categories = Category::all();
        foreach ($categories as $category) {

            $users = User::inRandomOrder()->limit(15)->get();
            foreach ($users as $user) {
                $ranking = new Ranking();
                $ranking->score = random_int(0,100);
                $ranking->user()->associate($user);
                $ranking->category()->associate($category);
                $ranking->save();
            }
        }
    }
}
