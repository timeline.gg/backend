<?php

use App\Category;
use Illuminate\Database\Seeder;
use Illuminate\Http\UploadedFile;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $image = new UploadedFile("database/data/multi.jpeg","multi.jpeg");
        $name = 'categories/' . uniqid() . '.' . $image->extension();
        $image->storePubliclyAs('public', $name);
        $category = new Category([
            'title'              => "Multitemático",
            'description'        => "Aquí se mezcla un poco de cada una las expansiones y tendrás acceso a diferentes ramas de la ciencia, música y entretenimiento.",
            'multi'              => true,
            'image'              => $name
        ]);
        $category->save();
        $image = new UploadedFile("database/data/inventions.jpeg","inventions.jpeg");
        $name = 'categories/' . uniqid() . '.' . $image->extension();
        $image->storePubliclyAs('public', $name);
        $category = new Category([
            'title'              => "Inventos",
            'description'        => "Si eres fanático de la creatividad y la inventiva, sería interesante conocer en qué momento se inventaron ciertos artefactos que forman parte de nuestra cotidianidad.",
            'multi'              => false,
            'image'              => $name
        ]);
        $category->save();
        $image = new UploadedFile("database/data/science.jpeg","science.jpeg");
        $name = 'categories/' . uniqid() . '.' . $image->extension();
        $image->storePubliclyAs('public', $name);
        $category = new Category([
            'title'              => "Ciencia y Descubrimientos",
            'description'        => "La ciencia y la tecnología están llenos de datos curiosos, por lo que, en esta expansión conocerás exactamente la fecha en que ocurrieron ciertos avances tecnológicos vinculados a las ciencias y los descubrimientos.",
            'multi'              => false,
            'image'              => $name
        ]);
        $category->save();
        $image = new UploadedFile("database/data/cinema.jpeg","cinema.jpeg");
        $name = 'categories/' . uniqid() . '.' . $image->extension();
        $image->storePubliclyAs('public', $name);
        $category = new Category([
            'title'              => "Música y Cine",
            'description'        => "Este juego incorpora datos interesantes acerca de elementos vinculados a la música, el arte, el entretenimiento y la televisión.",
            'multi'              => false,
            'image'              => $name
        ]);
        $category->save();
    }
}
