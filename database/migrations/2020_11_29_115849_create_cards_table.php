<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->id();
            //Para la imagen de la carta guardamos un campod e tipo string, que va a ser la URL de la imagen.
            $table->string('title');
            $table->string('description')->nullable();
            $table->integer('year');
            $table->string('image')
                  ->nullable();
            $table->unsignedBigInteger('category_id');
            // El timestamp guarda la fecha de modificación de los cambios.
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
