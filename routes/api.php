<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::get('signup/activate/{token}', 'AuthController@signupActivate');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::group(['middleware' => 'auth:api'], function() {
    
    Route::apiResource('users','UserController');

    Route::apiResource('categories', 'CategoryController')->only(["store","update","destroy"]);

    Route::get('categories/{category}/cards', 'CategoryController@cards')->name('categories.cards');

    Route::apiResource('cards', 'CardController')->only(["store", "show", "destroy", "update"]);
});

Route::apiResource('categories', 'CategoryController')->except(["store","update","destroy"]);
Route::get('game/categories', 'GameController@categories')->name("game.categories");
Route::get('game/start/{category}', 'GameController@start')->name("game.start");
Route::get('game/{game}/hand', 'GameController@hand')->name("game.hand");
Route::post('game/{game}/resolve', 'GameController@resolve')->name("game.resolve");
//RUTICA RANKING
Route::apiResource('rankings', 'RankingController')->except(["store","update","destroy"]);

// Route::apiResource('card', 'CardController')->except(["index","store","update","destroy"]);







