<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['title','description','image'];

    public function cards()
    {
        return $this->hasMany(Card::class);
    }

    public function games()
    {
        return $this->hasMany(Game::class);
    }

    public function rankings(){
        return $this->hasMany(Ranking::class);
    }
}
