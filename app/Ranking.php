<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ranking extends Model
{

    public $timestamps = false;

    function category() {
        return $this->belongsTo(Category::class);
    }

    function user() {
        return $this->belongsTo(User::class);
    }
}
