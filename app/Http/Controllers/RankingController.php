<?php

namespace App\Http\Controllers;

use App\Ranking;
use App\Category;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\RankingResource;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class RankingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $sortBy = $request->query('sortBy','title');
        // $sortDesc =  filter_var($request->query('sortDesc',false), FILTER_VALIDATE_BOOLEAN);
        // $perPage = $request->query('perPage',15);

        return RankingResource::collection(
            Category::all()->map(function( $category ){				
                $category->load(['rankings'=> function($query){
                    $query->orderBy('score','desc')->take(5);
                }]);
                return $category;}
            ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
