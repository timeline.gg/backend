<?php

namespace App\Http\Controllers;

use App\Game;
use Illuminate\Http\Request;
use App\Category;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CardResource;
use App\Http\Resources\GameResource;
use App\Card;
use App\Ranking;
use Facade\Ignition\Solutions\SuggestCorrectVariableNameSolution;
use Illuminate\Support\Carbon;

class GameController extends Controller
{

    const ROUND_TIME = 55;
    const HAND_CARDS_NUMBER = 5;
    const MAX_MISSES = 5;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categories()
    {   
        return CategoryResource::collection(
            Category::orderBy('title', 'asc')->get());
    }

    public function start(Request $request, Category $category){       

        $game = new Game();
        $game->user()->associate($request->user('api'));
        $game->category()->associate($category->id);
        $game->round = 0;
        $game->misses = 0;
        $game->score = 0;
        $game->save();

        return new GameResource($game);
    }

    public function hand(Request $request, Game $game) {
        
        // Si la categoría es multi, se cogen carta de todas las categorías
        // Las cartas se cogen en orden aleatorio limitado a 5 cartas
        if ( $game->category->multi) {
            $collection = Card::inRandomOrder()->limit(self::HAND_CARDS_NUMBER);
        } else {
            $collection = $game->category->cards()
                               ->inRandomOrder()->limit(self::HAND_CARDS_NUMBER);
        }

        // Obtenemos la mano de cartas
        $hand = $collection->get();

        // De la mano de cartas, construimos dos arrays, uno con los ids de las cartas 
        // y otro con el año de las cartas, correspondientes.
        $hand_cards = array_reduce($hand->toArray(),
            function($items, $item){
                $items['ids'][] = $item['id'];
                $items['years'][] = $item['year'];
                return $items;
            }, ['ids'=>[],'years'=>[]]);
        
        array_multisort($hand_cards['years'],SORT_NUMERIC,SORT_ASC,$hand_cards['ids']);
        
        $game->hand_cards = $hand_cards;
        $game->round++;
        $game->start_round_time = now();
        $game->save();

        return CardResource::collection($hand)
                                ->additional(['meta' => [
                                    'round' => $game->round,                                    
                                    'round_time' => self::ROUND_TIME
                                ]]);;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function resolve(Request $request, Game $game)
    {
        $this->validate($request, [   
            'round' => 'required'
        ]);

        $fields = $request->only(['round', 'answer']);
        if ( $game->round != $fields['round'] ) {
            return response()->json(["message"=>"Round mismatch."],409);
        }

        $remaining_time = self::ROUND_TIME - Carbon::now()->diffInSeconds($game->start_round_time);        
        
        $correct = ($fields['answer'] == $game->hand_cards["ids"]);
        
        $score = $game->score + (int) $correct * (55 + $remaining_time * 5);
        
        $misses = $game->misses + (int) !$correct;

        $data = [
            "correct_answer" => array_combine ( $game->hand_cards["ids"], $game->hand_cards["years"]),
            "correct" => $correct
        ];
        $meta = [
            "score"=>$score,
            "misses"=>$misses,
            "end"=> ($misses == self::MAX_MISSES)
        ];

        $ranking = null;
        if ($misses == self::MAX_MISSES) {
            if ( $request->user('api') ) {
                // Save ranking
                $ranking = new Ranking();
                $ranking->score = $score;
                $ranking->user()->associate($request->user('api'));
                $ranking->category()->associate($game->category);
                $ranking->save();
            }
            $game->delete();
        } else {
            $game->score = $score;
            $game->misses = $misses;
            $game->hand_cards = null;
            $game->save();
        }
        

        
        return response()->json(["data"=>$data,"ranking"=>$ranking,"user"=>$request->user('api'), "meta"=>$meta],200);
    }

}
