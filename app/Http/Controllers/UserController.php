<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\SignupActivate;
use Illuminate\Support\Str;
use SebastianBergmann\Environment\Console;
use Illuminate\Support\Facades\File; 


class UserController extends Controller
{

    //Función para obtener los datos del usuario. 
    public function show(Request $request, User $user)
    {
        //Obtengo al usuario autenticado
        $request->user()->authorizeRoles(['user', 'admin']);
        //En caso de no autenticado --> 
        return new UserResource($user);
        
    }

    public function update(Request $request, User $user){

        //Obtengo al usuario autenticado
        $request->user()->authorizeRoles(['user', 'admin']);
        //Si el usuario tiene el ROL
        if ( $request->user()->hasRole('user') ) {
            //Si el rol es 'user' y  la 'id' del usuario (en la request) no coincide con su id:
            if ($request->user()->id != $user->id) {
                //Retorno el estadod e error 418.
                return response()->json(["message"=>"I'm a teapot"],418);
            }
        }

        //Realizo una validación de las dimensiones de la imagen que se quiere actualizar
        $this->validate($request, [   
            'image' => ['image', 'dimensions:max_width=1000,max_height=1000']
        ]);

        //Obtengo los campos que se pueden actualizar de la request ('name' y 'email').
        $fields = $request->only(['name', 'email']);
        //Obtengola imagen
        $file = $request->file('image');
        //Si el archivo existe
        if($file) {
            //Primero elimino el que ya tenía el usuario
            File::delete(storage_path('app/public').$user->image);
            //Ahora guardo la nueva imagen
            $fields['image'] = $this->saveUserImage($file);
        }
        //Por útimo actualizo los datos del usuario
        $user->update($fields);
        return new UserResource($user);
    }

    /**
     * Saves a category image from the request.
     *
     * @param  \Illuminate\Http\UploadedFile|\Illuminate\Http\UploadedFile[]  $file
     * @return string
     */
    private function saveUserImage(\Illuminate\Http\UploadedFile $file) {
        //Preparo el nombre o ruta del archivo. Para ello empleo la función 'uniqid()' y concateno la extensión del archivo usando la función 'extension()'
        $name = 'users/' . uniqid() . '.' . $file->extension();
        //Los archivos en Laravel pueden ser públicos o privados.
        //Se puede cambiar la condición de visibilidad y la función a emplear dependerá de si trabajamos con el archivo guardado, o cargado en el momento.
        //Voy a emplear el método 'storePublicLyAs()' para almacenar el archivo cargado con visibilidad pública.
        $file->storePubliclyAs('public', $name);
        return $name;
    }

    public function index (Request $request) {

        //Obtengo al usuario autenticado
        $request->user()->authorizeRoles(['admin']);

        //Preparo variables de paginación, ordenación y filtro
        $sortBy = $request->query('sortBy','name');
        $sortDesc =  filter_var($request->query('sortDesc',false), FILTER_VALIDATE_BOOLEAN);
        $perPage = $request->query('perPage',15);
        $filter = $request->query('filter',null);
        //Obtengo colección desde el modelo con orden 
        $collection = User::orderBy($sortBy, $sortDesc ? 'desc':'asc');
        //Si hay filtro aplico condicional de filtrado
        if(!is_null($filter)) {
            $collection->where('name','ilike','%'.$filter.'%')
                        ->orWhere('email','ilike','%'.$filter.'%');
        }
        //Retorno una colección de recursos paginada
        return UserResource::collection($collection->paginate($perPage));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
        //Obtengo al usuario autenticado
        $request->user()->authorizeRoles(['admin']);        
        if ($user->id==1) {
            return response()->json(["message"=>"I'm a teapot"],418);
        }
        if ($user->image) {
            File::delete(storage_path('app/public').$user->image);
        }
        $user->delete();

        return response()->json(null,204);
    }

    
}
