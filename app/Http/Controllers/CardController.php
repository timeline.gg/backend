<?php

namespace App\Http\Controllers;

use App\Card;
use App\Category;
use App\Http\Resources\CardResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File; 

class CardController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'year' => 'required',
            'category_id' => 'required|exists:App\Category,id',
            'image' => [ 'required', 'image', 'dimensions:max_width=1000,max_height=1000']
        ]);

        //Ahora guardo el archivo mediante el método 'file'. Ésto me proporciona una variedad de métodos para interactuar con el archivo.
        $card = new Card();
        $file = $request->file('image');
        if($file) {
            $card->image = $this->saveCardImage($file);
        } else {
            $card->image = null;
        }
        
        $card->title = $request->title;
        $card->description = $request->description;
        $card->year = $request->year;
        $card->category()->associate($request->category_id);
        $card->save();
        $card->load('category');
        return new CardResource($card);
    }

    /**
     * Saves a category image from the request.
     *
     * @param  \Illuminate\Http\UploadedFile|\Illuminate\Http\UploadedFile[]  $file
     * @return string
     */
    private function saveCardImage(\Illuminate\Http\UploadedFile $file) {
        //Preparo el nombre o ruta del archivo. Para ello empleo la función 'uniqid()' y concateno la extensión del archivo usando la función 'extension()'
        $name = 'cards/' . uniqid() . '.' . $file->extension();
        //Los archivos en Laravel pueden ser públicos o privados.
        //Se puede cambiar la condición de visibilidad y la función a emplear dependerá de si trabajamos con el archivo guardado, o cargado en el momento.
        //Voy a emplear el método 'storePublicLyAs()' para almacenar el archivo cargado con visibilidad pública.
        $file->storePubliclyAs('public', $name);
        return $name;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function show(Card $card)
    {
        $card->load("category");
        return new CardResource($card);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Card $card)
    {
        $request->user()->authorizeRoles(['admin']);

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'year' => 'required',
            'image' => ['image', 'dimensions:max_width=1000,max_height=1000']
        ]);

        $fields = $request->only(['title', 'description', 'year']);
        $file = $request->file('image');
        if($file) {
            File::delete(storage_path('app/public').$card->image);
            $fields['image'] = $this->saveCardImage($file);
        }
        
        $card->update($fields);
        
        $card->load('category');
        return new CardResource($card);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Card  $card
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Card $card)
    {
        $request->user()->authorizeRoles(['admin']);
        if ($card->image) {
            File::delete(storage_path('app/public').$card->image);
        }
        $card->delete();

        return response()->json(null,204);
    }
}
