<?php

namespace App\Http\Controllers;

use App\Category;
use App\Card;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CardResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File; 


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sortBy = $request->query('sortBy','title');
        $sortDesc =  filter_var($request->query('sortDesc',false), FILTER_VALIDATE_BOOLEAN);
        $perPage = $request->query('perPage',15);
        
        return CategoryResource::collection(
            Category::withCount('cards')
                ->orderBy($sortBy, $sortDesc ? 'desc':'asc')
                ->paginate($perPage));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->user()->authorizeRoles(['admin']);

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',    
            'image' => ['image', 'dimensions:max_width=1000,max_height=1000']
        ]);

        $category = new Category();

        //Ahora guardo el archivo mediante el método 'file'. Ésto me proporciona una variedad de métodos para interactuar con el archivo.

        $file = $request->file('image');
        if($file) {
            $category->image = $this->saveCategoryImage($file);
        } else {
            $category->image = null;
        }
        
        $category->title = $request->title;
        $category->description = $request->description;
        $category->multi = false;
        $category->save();

        return new CategoryResource($category);
    }

    /**
     * Saves a category image from the request.
     *
     * @param  \Illuminate\Http\UploadedFile|\Illuminate\Http\UploadedFile[]  $file
     * @return string
     */
    private function saveCategoryImage(\Illuminate\Http\UploadedFile $file) {
        //Preparo el nombre o ruta del archivo. Para ello empleo la función 'uniqid()' y concateno la extensión del archivo usando la función 'extension()'
        $name = 'categories/' . uniqid() . '.' . $file->extension();
        //Los archivos en Laravel pueden ser públicos o privados.
        //Se puede cambiar la condición de visibilidad y la función a emplear dependerá de si trabajamos con el archivo guardado, o cargado en el momento.
        //Voy a emplear el método 'storePublicLyAs()' para almacenar el archivo cargado con visibilidad pública.
        $file->storePubliclyAs('public', $name);
        return $name;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return new CategoryResource($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $request->user()->authorizeRoles(['admin']);
        
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',   
            'image' => ['image', 'dimensions:max_width=1000,max_height=1000']
        ]);

        $fields = $request->only(['title', 'description']);
        $file = $request->file('image');

        if($file) {
            File::delete(storage_path('app/public').$category->image);            
            $fields['image'] = $this->saveCategoryImage($file);
            
        }

        $category->update($fields);
        
        return new CategoryResource($category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Category $category)
    {
        $request->user()->authorizeRoles(['admin']);
        if ($category->image) {
            File::delete(storage_path('app/public').$category->image);
        }
        $category->delete();

        return response()->json(null,204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function cards(Request $request, Category $category) {

        $request->user()->authorizeRoles(['admin']);        

        $sortBy = $request->query('sortBy','title');
        $sortDesc =  filter_var($request->query('sortDesc',false), FILTER_VALIDATE_BOOLEAN);
        $perPage = $request->query('perPage',15);
        $filter = $request->query('filter',null);

        $collection = $category->cards()
                               ->orderBy($sortBy, $sortDesc ? 'desc':'asc');
        
        if(!is_null($filter)) {
            // $collection->whereHas('card',function($q) use ($filters){
            //     return $q->where('state_id','=',$filters['state_id']);
            // });
            $collection->where('title','ilike','%'.$filter.'%')
                       ->orWhere('year','ilike','%'.$filter.'%')
                       ->orWhere('description','ilike','%'.$filter.'%');
        }
                
        return CardResource::collection($collection->paginate($perPage));
            
    }
}
