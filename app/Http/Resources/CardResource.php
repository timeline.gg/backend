<?php

namespace App\Http\Resources;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class CardResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'image' => $this->image? asset(Storage::url($this->image)) : null,
            'category_id' => 2,
            'year'=> $this->year,
            $this->mergeWhen(Auth::user() && Auth::user()->hasRole('admin'), [
                'year'=> $this->year,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ])
            
        ];
    }
}
