<?php

namespace App\Http\Resources;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;


class CategoryResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        // $data = parent::toArray($request);
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'image' => $this->image? asset(Storage::url($this->image)) : null,
            'multi' => $this->multi,
            'cards_count' => $this->cards_count,
            $this->mergeWhen(Auth::user() && Auth::user()->hasRole('admin'), [
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ])
            
        ];       
    }
}
