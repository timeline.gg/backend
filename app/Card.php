<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{

    protected $fillable = ['title','description','image', 'year'];

    function category() {
        return $this->belongsTo(Category::class);
    }
}
